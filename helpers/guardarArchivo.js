const fs = require('fs');

const ruta = './db/data.json';

const guardarDB = ( data ) => {
    fs.writeFileSync(ruta,JSON.stringify(data));
}

const leerDB = () => {

    if( !fs.existsSync(ruta)){
        return null;
    }
    const info = JSON.parse(fs.readFileSync(ruta, { encoding: 'utf-8'}));
    return info;
}

module.exports = {
    guardarDB,
    leerDB
}