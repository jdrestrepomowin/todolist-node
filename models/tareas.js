const Tarea = require("./tarea");

/* 
    _listado:
        { 'uuid-122344-22333-2: { id:12, desc:asd, completadoEn:923456}}

*/
class Tareas {
    _listado = {};

    get listadoArr(){
        const listado = [];
        Object.keys(this._listado).forEach( key => {
            const tarea = this._listado[key];
            listado.push( tarea );
        })
        return listado;
    }

    constructor() {
        this._listado = {};
    }

    borrarTarea( id=''){
        if(this._listado[id]){
            delete this._listado[id];
        }
    }

    crearTarea(desc = ''){
        const tarea = new Tarea(desc);
        this._listado[tarea.id] = tarea;
    }

    cargarTareasFromArray(tareas){
        tareas.forEach( tarea => {
            this._listado[tarea.id] = tarea
        })
    }

    listadoCompleto(){
        console.log();
        this.listadoArr.forEach((tarea, index) =>{
                if(tarea.completadoEn != null){
                    console.log(`${index+1}. ${tarea.desc}: ${tarea.completadoEn.green}`);
                }else{
                    console.log(`${index+1}. ${tarea.desc}: ${"Pendiente".red}`); 
                }
            }
        )
    }

    listarPendientesCompletadas( Completadas=true){
        this.listadoArr.filter( tarea => {
           return Completadas?tarea.completadoEn!=null:tarea.completadoEn===null
       }).forEach((tarea, index) => {
           Completadas?console.log(`${index+1}. ${tarea.desc}`.green)
                      :console.log(`${index+1}. ${tarea.desc}`.red)
       })
       
    } 
    
    toggleCompletadas( ids= []) {
        ids.forEach( id => {
            const tarea = this._listado[id];
            if (!tarea.completadoEn) {
                tarea.completadoEn = new Date().toISOString();
            }
        });

        this.listadoArr.forEach( tarea => {
            if (!ids.includes(tarea.id)) {
                this._listado[tarea.id].completadoEn = null;
            }
        })
    }
}

module.exports = Tareas;